package me.duzhi.ilog.cms.hok;

/**
 * Hok invoke
 *
 * @author ashang.peng@aliyun.com
 * @date 二月 14, 2017
 */

public class HokInvoke {

    /**
     * @author ashang.peng@aliyun.com
     * @date 二月 14, 2017
     */
    public static interface Inv<T> {
        public T invoke(Message message);
    }

    public static class Message {
        Object data;

        public Object getData() {
            return data;
        }

        public void setData(Object data) {
            this.data = data;
        }
    }


    /*
    public static void main(String[] args) {
        Boolean b = invoke(new Inv<Boolean>() {
            @Override
            public Boolean invoke() {
                return false;
            }
        }, "aa");
        System.out.println(b);
    }
    */


}
